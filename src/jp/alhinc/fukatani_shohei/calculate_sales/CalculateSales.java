package jp.alhinc.fukatani_shohei.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class CalculateSales {
	public static void main(String[] args) {
        try {
            Map<String, String> names = new HashMap<>();
            Map<String, Long> sale = new HashMap<>();

            File branchLst = new File(args[0], "branch.lst");

            if(!branchLst.exists()) {
                System.out.println("支店定義ファイルが存在しません");
                return;
            }

            BufferedReader br = null;
            try {
                br = new BufferedReader(new FileReader(branchLst));
                String line;
                while ((line = br.readLine()) != null) {
                    String[] words = line.split(",");

                    if(!words[0].matches("\\d{3}") || words.length != 2) {
                        System.out.println("支店定義ファイルのフォーマットが不正です");
                        return;
                    }

                    names.put(words[0], words[1]);
                    sale.put(words[0], 0l);
                }
            } finally {
                if(br != null) {
                    br.close();
                }
            }

            FilenameFilter filter = new FilenameFilter() {
                public boolean accept(File directory, String filename) {
                    return new File(directory, filename).isFile() && filename.matches("\\d{8}.rcd$");
                }
            };

            try {
                File directory = new File(args[0]);
                File rcdFiles[] = directory.listFiles(filter);

                List<Integer> rcdNumbers = new ArrayList<>();
                for (File rcdFile : rcdFiles) {
                    rcdNumber.add(Integer.parseInt(rcdFile.getName().substring(0, 8)));
                }

                Collections.sort(rcdNumbers);

                int min = rcdNumbers.get(0);
                int max = rcdNumbers.get(rcdNumbers.length - 1);
                int number = rcdNumbers.length - 1;
                if (number != max - min) {
                    System.out.println("売上ファイル名が連番になっていません");
                    return;
                }

                for (File rcdFile : rcdFiles) {
                    br = new BufferedReader(new FileReader(rcdFiles));
                    String cd = br.readLine();
                    Long price = Long.parseLong(br.readLine());
                    String err = br.readLine();

                    if(err != null) {
                        System.out.println(files[i] + "のフォーマットが不正です");
                        return;
                    }

                    if(!sale.containsKey(cd)) {
                        System.out.println(files[i] + "の支店コードは不正です。");
                        return;
                    }
                    Long total = price + sale.get(cd);
                    String totalstr = String.valueOf(total);
                    if(totalstr.length > 10) {
                        System.out.println("合計金額が10桁を超えました");
                        return;
                    }

                    sale.put(cd, total);
                }
            } finally {
                if(br != null) {
                    br.close();
                }
            }

            File branchOut = new File(args[0], "branch.out");
            try {
                BufferedWriter bw = new BufferedWriter(new FileWriter(branchOut));

                for(Entry<String, String> entry : names.entrySet()) {
                    String key = entry.getKey() ;
                    Long price = sale.get(key);
                    bw.write(key + "," + entry.getValue() + "," + price + "\r\n");
                }
            } finally {
                if(br != null) {
                    br.close();
                }
            }

        } catch (Exception e) {
            System.out.println("予期せぬエラーが発生しました。");
        }
	}
}
